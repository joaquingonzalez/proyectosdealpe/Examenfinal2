DROP database IF EXISTS ejercicio11;
CREATE DATABASE ejercicio11;
USE ejercicio11;

CREATE TABLE cursillo(
  id int AUTO_INCREMENT,
  nombre varchar(250),
  descripcion varchar(250),
  espersonalizado varchar(2),

  PRIMARY KEY (id)
  );

CREATE TABLE profesor(
  dni varchar(10),
  curriculum varchar(250),
  email varchar(250),
  telefono1 int(12),
  telefono2 int(12),
  direccion varchar(250),
  nombre varchar(250),
  apellido1 varchar(250),
  apellido2 varchar(250),
  cuentabancaria varchar(24),
  id_centro int,
  esempleado varchar(2),

  PRIMARY KEY (dni)
  );

CREATE TABLE alumno(
  dni varchar(10),
  email varchar(250),
  telefono1 int(12),
  telefono2 int(12),
  direccion varchar(250),
  nombre varchar(250),
  apellido1 varchar(250),
  apellido2 varchar(250),
  cuentabancaria varchar(24),
  id_centro int,
  esempleado varchar(2),

  PRIMARY KEY (dni)
  );

CREATE TABLE clase(
  id int AUTO_INCREMENT,
  fecha date,
  horainicio varchar(20),
  duracion int(12),
  dniprofesor varchar(10),
  idubicacion int,
  idedicion int,

  PRIMARY KEY (id)
  );

CREATE TABLE ubicacion(
  id int AUTO_INCREMENT,
  direccion varchar(250),
  propia varchar(20),
  descripcion varchar(250),


  PRIMARY KEY (id)
  );

CREATE TABLE centro(
  id int AUTO_INCREMENT,
  direccion varchar(250),
  telefono1 int(12),
  telefono2 int(12),
  obsoleta varchar(2),

  PRIMARY KEY (id)
  );

CREATE TABLE edicion(
  id int AUTO_INCREMENT,
  idcursillo int,
  fechapresentacion date,

  PRIMARY KEY (id)
  );

CREATE TABLE matricula(
  idedicion int,
  idalumno varchar(10),

  PRIMARY KEY (idedicion, idalumno)
  );

CREATE TABLE falta(
  idalumno varchar(10),
  idclase int,

  PRIMARY KEY (idalumno, idclase)
  );



ALTER TABLE edicion
  ADD CONSTRAINT fkedicion_cursillo FOREIGN KEY(idcursillo)
  REFERENCES cursillo(id) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE profesor
  ADD CONSTRAINT fkprofesor_centro FOREIGN KEY(id_centro)
  REFERENCES centro(id) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE clase
  ADD CONSTRAINT fkclase_profesor FOREIGN KEY(dniprofesor)
  REFERENCES profesor(dni) ON DELETE RESTRICT ON UPDATE CASCADE,

  ADD CONSTRAINT fkclase_ubicacion FOREIGN KEY(idubicacion)
  REFERENCES ubicacion(id) ON DELETE RESTRICT ON UPDATE CASCADE,

  ADD CONSTRAINT fkclase_edicion FOREIGN KEY(idedicion)
  REFERENCES edicion(id) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE matricula
  ADD CONSTRAINT fkmat_edicion FOREIGN KEY(idedicion)
  REFERENCES edicion(id) ON DELETE RESTRICT ON UPDATE CASCADE,

  ADD CONSTRAINT fkmat_alumno FOREIGN KEY(idalumno)
  REFERENCES alumno(dni) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE falta
  ADD CONSTRAINT fkfalta_alumno FOREIGN KEY(idalumno)
  REFERENCES alumno(dni) ON DELETE RESTRICT ON UPDATE CASCADE,

  ADD CONSTRAINT fkfalta_clase FOREIGN KEY(idclase)
  REFERENCES clase(id) ON DELETE RESTRICT ON UPDATE CASCADE;



/** EJERCICIO 12 **/
  
  CREATE OR REPLACE VIEW ejercicio12a AS
     SELECT * 
       FROM profesor p
       WHERE p.esempleado='si';  

  CREATE OR REPLACE VIEW ejercicio12b AS
     SELECT *
       FROM profesor p
       WHERE p.esempleado='no'
             AND p.id_centro IS NULL;  

    CREATE OR REPLACE VIEW ejercicio12c AS
     SELECT c.direccion
       FROM centro c
       WHERE c.obsoleta='ex';
    
    /** ejercicio 13**/
     
DROP PROCEDURE IF EXISTS ejercicio13a;
CREATE PROCEDURE ejercicio13a()
  BEGIN

    DROP TABLE IF EXISTS profemp;

    CREATE TABLE profemp AS 
        SELECT *
          FROM profesor p
          WHERE p.esempleado='si';
      
  END;  

DROP PROCEDURE IF EXISTS ejercicio13b;
CREATE PROCEDURE ejercicio13b()
  BEGIN

    DROP TABLE IF EXISTS profind;

    CREATE TABLE profind AS 
        SELECT *
          FROM profesor p
          WHERE p.esempleado='no';
      
  END;  

/** ej 14 **/

  DROP FUNCTION IF EXISTS ejercicio14a;
CREATE FUNCTION ejercicio14a(nota float)
  RETURNS varchar(20)
  BEGIN
      DECLARE resultado varchar(20);
   
      IF (nota<5) THEN 
          set resultado='SUSPENSO';
      ELSEIF (nota<6) THEN 
          set resultado='APROBADO';
      ELSEIF (nota<7) THEN 
          set resultado='BIEN';
      ELSEIF (nota<9) THEN 
          set resultado='NOTABLE';
      ELSEIF (nota<10) THEN 
          set resultado='SOBRESALIENTE';
      ELSE 
          set resultado='MATRICULA DE HONOR';
      END IF;
   
      RETURN resultado;
   END;

SELECT ejercicio14a(4.5);
SELECT ejercicio14a(5);
SELECT ejercicio14a(5.5);
SELECT ejercicio14a(6);
SELECT ejercicio14a(6.5);
SELECT ejercicio14a(6.9);
SELECT ejercicio14a(7);
SELECT ejercicio14a(7.1);
SELECT ejercicio14a(8.9);
SELECT ejercicio14a(9);
SELECT ejercicio14a(9.1);
SELECT ejercicio14a(9.9);
SELECT ejercicio14a(10);




